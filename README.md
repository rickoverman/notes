Notes & snippets
----------------

Read more notes:

* [Psychology](psychology.md)


----------------

## Set desktop brightness for HDMI output

    xrandr --output HDMI1 --brightness .7

## Download youtube

    youtube-dl -i https://www.youtube.com/watch?v=yIV1tcnOS80
